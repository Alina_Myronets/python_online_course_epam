**TASKS**
1. Open file `data/unsorted_names.txt` in data folder. Sort the names and write them to a new file called `sorted_names.txt`.
2. Implement a function which search for most common words in the file.
3. File `data/students.csv` stores information about students in CSV(https://en.wikipedia.org/wiki/Comma-separated_values) format.This file contains the student’s names, age and average mark. 1) Implement a function which receives file path and returns names of top performer students
2) Implement a function which receives the file path with students info and writes CSV student information to the new file in descending order of age. 



**TASK 1**

For solving thos task I just read our file 'unsorted_names'. Then I create a new file 'sorted_name', 
in mode `w+` for both reading and writing. After it, I sorted the names in our old file 'unsorted_names'  and using a loop, passed on each element of this file, and write it in my new file 'sorted names'.
After all work I closed this file, in order not to change anything else.

As a result I got such file .
[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/data/sorted_name.txt](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/data/sorted_name.txt)
The code of this task is here.
[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/Task10_1.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/Task10_1.ipynb)


**TASK 2**

For solving this task I used such modules, as `collections` and `re`.
From module collections imported `subclass Counter`,for counting hashable objects.
Module `re`-- this modulde provides regular expression matching operations.
в
I did the task as follows:
- Firstly, I read a file, using such fucntion `read()`.
- Next I led the entire text to lowercase by dint of Python string method lower(). 
- From module `re` I used method `.sub()` for gotting rid of unnecessary characters such as semicolons and so on. 
- Then, by  method `.split()` I splited my text(which is the data type string) into a list, where each word is a list item. 
- My next step was to count how many times each word occurs in the list, fot this I used subclass `Counter`.
- For returning `n` most commom elements in descending ordes I used one of methods from Counter `.most_common([n])`.
Then I appended each of this word to the empty list.

The code of this task is here.
[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/Task10_2.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/Task10_2.ipynb)


**TASK 3**

For this task I used such popular library as Pandas. In order to use it You Shoulld imported it as follows:
`import pandas as pd`.In short Pandas is a high-level data manipulation tool.
For reading our file I took advantage such fucntion `pd.read_csv`, as file format is `.csv`. So, after reading the file we work with DataFrame -- a two-dimensional structure consisting of columns and rows. 

**The first function - get_top_performers()**
- My next step was sorting the column that matches for `average mark`in descending order; for it I used such fucntion a `.sort_values()`.
- And finally I dispalyed the first `n` students by means of function `.head(n)`.
- All this name I appended to empty list. 
As a result I got the list of the names of 5 top performer students.

**The second fucntion - get_performers()**
For solving this task I did the similar actions.
- sorted the column that matches for `age`in descending order; for it I used such fucntion a `.sort_values()`.
- changed the indexing of students
As a resul I got two-dimensional structure consisting of columns and rows, where such column as `age` is sorted in descending order.

The code of this task is here.
[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/Task10_3.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%2010/Task10_3.ipynb)

 