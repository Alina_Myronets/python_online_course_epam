Ta s k   1
Implement a function which receives a string and replaces all `"` symbols with `'` and vise versa.
Program to the first task.[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_1.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_1.ipynb)

Ta s k   2
Write a function that check whether a string is a palindrome or not.
Program to the first task.[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_2.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_2.ipynb)

Ta s k   3
Implement a function `get_shortest_word(s: str) -> str` which returns the shortest word in the given string. The word can contain any symbols except whitespaces (` `, `\n`, `\t` and so on). If there are multiple shortest words in the string with a same length return the word that occurs first. Usage of any split functions is forbidden.
Program to the first task.[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_3.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_3.ipynb)

Ta s k   4
Implement a bunch of functions which receive a changeable number of strings and return next parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
4) characters of alphabet, that were not used in any stringNote: use `string.ascii_lowercase` for list of alphabet letters.
Program to the first task.[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_4.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_4.ipynb)

Ta s k   5
Implement a function, that takes string as an argument and returns a dictionary, that contains letters of given string as keys and a number of their occurrence as values.
Program to the first task.[https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_5.ipynb](https://gitlab.com/Alina_Myronets/python_online_course_epam/-/blob/master/Task%209/Task9_5.ipynb)

